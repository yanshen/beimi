# 深圳贝密科技有限公司

#### 介绍
最新版 0.8.0
贝密游戏已经在二次开发了，还有很多功能在完善，已经修改了一部分了，开源棋牌游戏，包含麻将、斗地主。首个版本会采用当前最流行的房卡模式。
贝密游戏是一系列棋牌游戏的名称，其中包含麻将、斗地主、德州，目前正在进行UI设计以及后台系统（JAVA）开发，7月中发布0.1.0版本，仅包含前端UI方案，预计8月份发布首个版本，敬请关注！
 1.开发工具：Cocos Creater
 2.开发语言：Java + JavaScript
 3.服务端框架：Spring Boot + MySQL + JPA + TIO 
 4.客户端语言：Cocos Creater/JavaScript


 ** 欢迎加入棋牌游戏开发QQ群541037369，里面都是资深棋牌游戏开发者，一起打造更优秀的棋牌游戏！** 


软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/110322_9757ddc7_4842091.jpeg "UDLUFZDDT1OKK6VQL)KCW7C.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/105621_1158fb1d_4842091.png "192826_1d6f397f_1387891.png")
#### 安装教程
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/105641_7a15423f_4842091.png "192845_5526c6bf_1387891.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/105739_4eee3a58_4842091.jpeg "131509_9a969010_1387891.jpg")

编译调试部署说明

1. 服务器编译

安装调试的前提要求
必须安装了 maven，和 MySQL。

修改数据库连接地址
修改 src/main/resources/application.properties 文件下面三个地方，改成自己的MySQL服务器的ip地址和帐号密码
spring.datasource.url=jdbc:mysql://localhost:3306/beimi?useUnicode=true&characterEncoding=UTF-8 spring.datasource.username=root spring.datasource.password=

用 maven 安装两个依赖包
这两个包已经在项目中，不需要下载，执行下面命令即可。
mvn install:install-file -Dfile=src/main/resources/WEB-INF/lib/jave-1.0.2.jar -DgroupId=lt.jave -DartifactId=jave -Dversion=1.0.2 -Dpackaging=jar
mvn install:install-file -Dfile=src/main/resources/WEB-INF/lib/ip2region-1.2.4.jar -DgroupId=org.lionsoul.ip2region -DartifactId=ip2region -Dversion=1.2.4 -Dpackaging=jar

编译代码
mvn compile

运行服务器端
mvn spring-boot:start
可以如下面这样，在运行时添加一些环境变量
mvn spring-boot:start -Drun.jvmArguments='-Dserver.port=8080'

编译打包服务器端
mvn package
打包完成后会在项目根目录生成一个 target 目录，里面的 beimi-0.7.0.war 文件是打包完成的文件。

运行编译后war
java -Xms1240m -Xmx1240m -Xmn450m -XX:PermSize=512M -XX:MaxPermSize=512m -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+UseTLAB -XX:NewSize=128m -XX:MaxTenuringThreshold=0 -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=60 -XX:+PrintGCDetails -Xloggc:gc.log -jar target/beimi-0.7.0.war

2. 客户端编译流程
客户端代码位置在： client/version/chess

打开客户端项目
用 CocosCreater 打开 client/version/chess 项目。
----用 CocosCreater 创建一个空白新项目 删除 assets和settings两个目录，将 client/version/chess 下的 assets 和 settings 目录cp不过去。----

修改服务器ip地址
修改 client/version/chess/assets/resources/script/lib/HTTP.js 文件里的下面两句，改成自己的IP和端口号
baseURL:"http://localhost", wsURL : "http://localhost:9081",

启动 CocosCreater 调试
然后用 CocosCreater 运行。

3. 基于 docker 编译

编译 docker 镜像前的准备
数据库连接地址需要改成 beimi，这个名字是在 docker-compose.yml 里设置的一个 docker 别名。（在 application.properties 里改）
docker 启动后 mysql 数据库需要初始化数据结构和内容，初始化文件在 script/beimi.sql 。这个做一次即可，整个mysql容器的数据都存在 docker/mysql/data 目录下。只要这个目录不删除数据一直存在。可以反复重启或者删除docker容器数据都会存在。
编译 docker 镜像之前必须使用 CocosCreater 发布一下项目，而且需要使用手机版发布。如果没有用手机版发布，需要到docker-compose.yml里，找到 beimi-client > volumes 标签，由 web-mobile 换成 desktop。

执行编译镜像
mvn clean package docker:build

启动docker容器（包括，mysql，beimi server,beimi client共三个容器）
docker-compose up -d

关闭并删除所有容器
docker-compose down
注意：docker-compose down 并不会删除数据库内容，因为数据库内容存在目录里。

#### 使用说明

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

大家的支持才是我前进动力，希望能打造最优秀的棋牌游戏！
![输入图片说明](https://images.gitee.com/uploads/images/2019/0321/093945_87761308_4842091.png "L1`W_UH%_4}`(LT$N}V7I%L.png")
#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)